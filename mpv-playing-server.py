#!/bin/env python3
import argparse
import logging
import re
from typing import Dict, List, Optional

from mpv_playing.ipc import main as ipc_main, AbstractRequestHandler, Observable, Request, StandardEvent
from mpv_playing.httpd import Server, set_server_version, HOLDER

__version__ = "0.1"
logger = logging.getLogger(__name__)


class RequestHandler(AbstractRequestHandler):
    def __init__(self, communicator):
        super(RequestHandler, self).__init__(communicator)

    def handle_response(self, request_type: Request, response: Dict):
        data = response["data"]
        logger.debug("Response for %s: %s", request_type, data)
        if request_type == Request.GetMediaTitle:
            HOLDER.get()._title = data
        elif request_type == Request.GetMediaLength:
            HOLDER.get()._length = int(data)
        elif request_type == Request.GetMediaPosition:
            HOLDER.get()._position = int(data)
        elif request_type == Request.GetIsPaused:
            HOLDER.get()._isPlaying = not data

    def handle_event(self, event_type: Optional[Observable], response: Dict):
        if event_type != Observable.EventPosition:
            logger.debug("Event for %s: %s", event_type, response)

        if event_type is None:
            event = StandardEvent.optional(response["event"])
            if event == StandardEvent.UnPause:
                HOLDER.get()._isPlaying = True

            elif event == StandardEvent.Pause:
                HOLDER.get()._isPlaying = False

            elif event == StandardEvent.MetadataUpdate:
                self.communicator.request(Request.GetMediaTitle)
                self.communicator.request(Request.GetMediaLength)

        elif event_type == Observable.EventPosition:
            data = response["data"]
            HOLDER.get()._position = int(data) if data is not None else 0
        pass

    def handle_connection_lost(self):
        HOLDER.get()._isPlaying = False


LOG_LEVELS = (
    'CRITICAL',
    'ERROR',
    'WARNING',
    'INFO',
    'DEBUG',
)


def parse_arguments(*args):
    parser = argparse.ArgumentParser()

    parser.add_argument("--log", type=str, action="append", default=[],
                        help="Configure log levels. If given only a level name, it will set level for root logger."
                             " Setting level for named logger is done like logger_name:log_level. Available"
                             " levels are {}".format(", ".join(LOG_LEVELS)))
    parser.add_argument("socket", metavar="SOCKET", type=str, help="mpv IPC socket filename.")

    args = None if len(args) == 0 else args

    return parser, parser.parse_args(args=args)


def configure_logger(parser, log_args: List[str]):
    default = None
    loggers = {}
    for log_arg in log_args:
        log_conf = re.match(r"(?:(?P<logger>.+):)?(?P<level>\w+)", log_arg)
        if log_conf is None:
            parser.error("--log argument must be either a name of a level or logger_name:level_name."
                         " Valid names are {}".format(", ".join(LOG_LEVELS)))
        level = log_conf.group("level").upper()
        if level not in LOG_LEVELS:
            parser.error("Invalid --log level name. Valid names are {}".format(", ".join(LOG_LEVELS)))

        name = log_conf.group("logger")
        if name is None:
            if default is not None:
                logger.warning("Multiple default logging levels defined.")
            default = level
        else:
            if name in loggers:
                logger.warning("Multiple logging levels defined for %s.", name)
            loggers[name] = level

    if default is None:
        default = logging.ERROR

    logging.basicConfig(level=default)

    for name, level in loggers.items():
        logging.getLogger(name).setLevel(level)


def main(*argv):
    parser, args = parse_arguments(*argv)
    configure_logger(parser, args.log)

    set_server_version(__version__)
    print("mpv-playing v{}".format(__version__))

    s = Server()
    s.start()
    ipc_main(args.socket, request_handler=RequestHandler)
    s.shutdown()
    s.join()


if __name__ == "__main__":
    main()
