
def format_time(seconds: int) -> str:
    minutes, seconds = divmod(seconds, 60)
    if minutes < 60:
        return "{:02}:{:02}".format(minutes, seconds)
    hours, minutes = divmod(minutes, 60)
    return "{}:{:02}:{:02}".format(hours, minutes, seconds)
