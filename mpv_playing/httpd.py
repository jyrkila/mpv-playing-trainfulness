#!/bin/env python3

import http.server
import logging
import threading

import jinja2
from jinja2 import Environment, PackageLoader

from .utils import format_time

logger = logging.getLogger(__name__)


class CurrentlyPlayingInformation(object):
    def __init__(self, title: str, length: int):
        self._title = title
        self._length = length
        self._position = 0
        self._isPlaying = False

    @property
    def title(self) -> str:
        return self._title

    @property
    def length(self) -> int:
        return self._length

    @property
    def position(self) -> int:
        return self._position

    @property
    def isPlaying(self):
        return self._isPlaying


class InformationHolder(object):
    def __init__(self):
        self._lock = threading.Semaphore()
        self._information = CurrentlyPlayingInformation("(none)", 0)

    def set(self, information: CurrentlyPlayingInformation):
        with self._lock:
            self._information = information

    def get(self) -> CurrentlyPlayingInformation:
        with self._lock:
            return self._information


HOLDER = InformationHolder()


def set_server_version(version: str):
    ResponseHandler.server_version = "mpv_playing/{}".format(version)


class ResponseHandler(http.server.BaseHTTPRequestHandler):
    server_version = "mpv_playing"

    def __init__(self, request, client_address, server):
        self._environ = Environment(
            loader=PackageLoader("mpv_playing"),
            autoescape=jinja2.select_autoescape(),
        )
        self._environ.filters["duration"] = format_time

        # This actually does the handling, so initialize last :(
        super().__init__(request, client_address, server)

    def send_404(self):
        self.send_error(404)

    def do_GET(self):
        if self.path != "/":
            self.send_404()
            return
        info = HOLDER.get()
        response = self.render("index.html", dict(
            title=info.title,
            position=info.position,
            length=info.length,
            version=self.server_version,
            isPlaying=info.isPlaying
        ))
        r_data = response.encode("utf-8")
        r_len = len(r_data)
        self.send_response(200, "OK", result_length=r_len)
        self.send_header("Content-Length", r_len)
        self.send_header("Content-Type", "text/html; charset=utf-8")
        self.end_headers()
        self.wfile.write(r_data)

    def render(self, template_name, args) -> str:
        template = self._environ.get_template(template_name)
        return template.render(**args)

    def send_response(self, code, message=None, result_length=None):
        # Otherwise a copy of super, but with added size argument to the log_request.
        self.log_request(code, size="-" if result_length is None else result_length)
        self.send_response_only(code, message)
        self.send_header('Server', self.version_string())
        self.send_header('Date', self.date_time_string())


class Server(threading.Thread):
    def __init__(self):
        super().__init__(name="WebServer")
        self._server = None  # type: http.server.HTTPServer

    def run(self):
        address = ('', 8000)
        self._server = http.server.HTTPServer(address, ResponseHandler)
        self._server.serve_forever()
        self._server.server_close()

    def shutdown(self):
        if self._server is not None:
            print("Shutting down...")
            self._server.shutdown()


def main():
    s = Server()
    s.start()
    try:
        s.join()
    except KeyboardInterrupt:
        pass
    finally:
        s.shutdown()


if __name__ == "__main__":
    main()
