import asyncio
import enum
import json
import logging
import os.path
import sys
from typing import Any, Dict, List, Optional

from .utils import format_time

logger = logging.getLogger(__name__)


@enum.unique
class RequestCommand(enum.Enum):
    GetProperty = "get_property"
    SetProperty = "set_property"
    Observe = "observe_property"
    UnObserver = "unobserve_property"


@enum.unique
class Observable(enum.IntEnum):
    """
    ID-values for RequestCommand.Observe.
    """
    EventPosition = 1


@enum.unique
class StandardEvent(enum.Enum):
    Pause = "pause"
    UnPause = "unpause"
    MetadataUpdate = "metadata-update"

    @classmethod
    def optional(cls, name: str) -> Optional["StandardEvent"]:
        try:
            return cls(name)
        except ValueError:
            return None


class Request(enum.Enum):
    GetMediaTitle = 1, RequestCommand.GetProperty, "media-title"
    GetFilename = 2, RequestCommand.GetProperty, "filename"
    GetMediaLength = 3, RequestCommand.GetProperty, "duration"
    GetMediaPosition = 4, RequestCommand.GetProperty, "time-pos"
    ObservePosition = 5, RequestCommand.Observe, Observable.EventPosition, "time-pos"
    GetIsPaused = 6, RequestCommand.GetProperty, "pause"

    # noinspection PyInitNewSignature
    def __init__(self, request_id: int, command: RequestCommand, *args: List[Any]):
        self.request_id = request_id
        self.command = command
        self.args = args
        if not hasattr(self.__class__, "_id_map"):
            self.__class__._id_map = {}
        self.__class__._id_map[request_id] = self

    @classmethod
    def get(cls, request_id):
        # noinspection PyProtectedMember
        return cls._id_map[request_id]

    @staticmethod
    def check(clazz):
        for k in Request:
            if not hasattr(clazz, k.name):
                raise KeyError("{} is not defined in {}".format(k.name, clazz))
        return clazz


# noinspection PyMethodMayBeStatic
class AbstractRequestHandler(object):
    def __init__(self, communicator: "Communicator.Proxy"):
        self.communicator = communicator

    def handle_response(self, request_type: Request, response: Dict):
        raise NotImplementedError

    def handle_event(self, event_type: Optional[Observable], response: Dict):
        raise NotImplementedError

    def handle_error(self, data: Dict):
        logger.warning("Request error: %s", data["error"])

    def handle_connection_lost(self):
        raise NotImplementedError


# noinspection PyPep8Naming,PyMethodMayBeStatic
@Request.check
class ConsoleRequestHandler(AbstractRequestHandler):
    def __init__(self, communicator):
        super(ConsoleRequestHandler, self).__init__(communicator)
        self._pos_end = "\x1b[G" if sys.stdout.isatty() else "\n"
        self._last_pos = None

    def handle_response(self, request_type: Request, response: Dict):
        getattr(self, request_type.name)(response["data"])

    def GetMediaTitle(self, data):
        print("New title:", data)

    def GetFilename(self, data):
        print("Filename:", data)

    def GetMediaLength(self, data):
        print("Length:", format_time(int(data)))

    def GetMediaPosition(self, data):
        print("Position:", format_time(int(data)))

    def GetIsPaused(self, data):
        print("Playing:", not data)

    def ObservePosition(self, data):
        pass

    def handle_event(self, event_type: Optional[Observable], response: Dict):
        if event_type is not None:
            getattr(self, event_type.name)(response)
        else:
            event = StandardEvent.optional(response["event"])
            if event == StandardEvent.Pause:
                print("Paused  ")
            elif event == StandardEvent.UnPause:
                if not sys.stdout.isatty():
                    print("Continue")

    def EventPosition(self, data):
        value = data["data"] or 0
        pos = format_time(int(value))
        if self._last_pos == pos:
            return
        self._last_pos = pos
        msg = "Position: {}".format(pos)
        print(msg, end=self._pos_end)
        sys.stdout.flush()

    def handle_connection_lost(self):
        pass


class Communicator(asyncio.Protocol):
    class Proxy(object):
        def __init__(self, comm: "Communicator"):
            self.__comm = comm

        def request(self, request: Request):
            self.__comm._request(request)

    def __init__(self, loop: asyncio.AbstractEventLoop,
                 end_event: asyncio.Event,
                 handler=None):
        self._loop = loop
        self._transport = None  # type: asyncio.Transport
        self._handler = handler(Communicator.Proxy(self)) or ConsoleRequestHandler(Communicator.Proxy(self))
        self._end_event = end_event
        self._shutting_down = False

    def connection_made(self, transport):
        self._transport = transport
        self._request(Request.GetMediaTitle)
        self._request(Request.GetMediaLength)
        self._request(Request.GetMediaPosition)
        self._request(Request.GetIsPaused)
        self._request(Request.ObservePosition)

    def data_received(self, data: bytes):
        # Multiple responses may be concatenated to one response.
        blobs = data.split(b'\n')
        for blob in blobs:
            if blob:
                try:
                    self._handle(blob)
                except:
                    logger.warning("Error handling data: %s", blob)
                    raise

    def _handle(self, data):
        try:
            d_obj = json.loads(data.decode())
        except json.JSONDecodeError as e:
            print(e, file=sys.stderr)
            print(data, file=sys.stderr)
            return
        if "request_id" in d_obj:
            r_id = d_obj["request_id"]
            if "error" in d_obj:
                if d_obj["error"] != "success":
                    self._handler.handle_error(d_obj)
                    return

            if "event" in d_obj:
                event_type = Observable(d_obj["id"]) if "id" in d_obj else None
                self._handler.handle_event(event_type, d_obj)
            elif "data" in d_obj:
                request_type = Request.get(r_id)
                self._handler.handle_response(request_type, d_obj)
        elif "event" in d_obj:
            event_type = Observable(d_obj["id"]) if "id" in d_obj else None
            self._handler.handle_event(event_type, d_obj)
        else:
            logger.warning("Data without id or event: %s", d_obj)

    def connection_lost(self, exc):
        if not self._shutting_down:
            logger.warning("Lost connection, exc=%s", exc)
        self._handler.handle_connection_lost()
        self._end_event.set()

    def _request(self, request: Request):
        command = [request.command.value]  # type: List[Any]
        command.extend(request.args)
        request = {
            "command": command,
            "request_id": request.request_id,
        }
        msg = json.dumps(request)
        msg += "\n"
        self._transport.write(msg.encode())

    def shutdown(self):
        if self._transport is not None:
            logging.debug("Closing transport in %r", self)
            self._shutting_down = True
            # Will eventually call connection_lost.
            self._transport.close()


class ConnectionHandler(object):
    def __init__(self, socket: str, loop: asyncio.AbstractEventLoop, request_handler=None):
        self._socket = socket
        self._loop = loop
        self._event = asyncio.Event()
        self._shutdown = False
        self._connection = None  # type: Communicator
        self._request_handler = request_handler

    def __call__(self):
        self._event.clear()
        self._connection = Communicator(self._loop, self._event, self._request_handler)
        logger.debug("Creating new connection %r", self._connection)
        return self._connection

    @asyncio.coroutine
    def start(self):
        was_connected = None  # type: bool
        while not self._shutdown:
            # Try to connect if socket exists.
            if os.path.exists(self._socket):
                coro = self._loop.create_unix_connection(self, self._socket)
                try:
                    yield from coro
                except (ConnectionRefusedError, FileNotFoundError) as e:
                    # Could not connect. Wait for a while.
                    if was_connected in (True, None):
                        logger.info("Connect failed: %s", e)
                    was_connected = yield from self._wait(was_connected)
                else:
                    # Connected. Run main handler by waiting for it to signal its end.
                    logger.info("Socket connected")
                    yield from self._event.wait()
                    self._connection.shutdown()
                    self._connection = None
                    was_connected = True
            else:
                was_connected = yield from self._wait(was_connected)

    @asyncio.coroutine
    def _wait(self, was_connected: bool) -> bool:
        if was_connected is None:
            print("mpv ipc not found in", self._socket)
            print("Start a mpv instance with --input-ipc-server={}".format(self._socket))
        elif was_connected:
            print("mpv ipc connection lost")
        yield from asyncio.sleep(0.5)
        return False

    def shutdown(self):
        logging.debug("Shutting down %r", self)
        self._shutdown = True
        if self._connection is not None:
            self._connection.shutdown()


def main(socket: str, request_handler=None):
    loop = asyncio.get_event_loop()

    handler = ConnectionHandler(socket, loop, request_handler)
    task = asyncio.Task(handler.start())
    try:
        loop.run_until_complete(task)
    except KeyboardInterrupt:
        handler.shutdown()
        # Make the co-routine complete its shutdown properly.
        loop.run_until_complete(task)
    finally:
        loop.close()


if __name__ == "__main__":
    main()
